import Home from '../pages/Home.vue';
import FilmDetail from '../pages/film/Detail.vue';

const routes = [
  { path: '/', name: 'home', component: Home, meta: { layout: ''} },
  { path: '/films/:idFilm', name: 'FilmDetail', component: FilmDetail, meta: { layout: ''} },
];

export default routes;
