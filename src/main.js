// Package
import Vue from 'vue'
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import BootstrapVue from 'bootstrap-vue'

import mainRoute from './router/main.js'
import store from './vuex/store';

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(Vuex);

// Css
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Global Component / Pages
import App from './App.vue'

const routes = [
  ...mainRoute,
]
const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

window.vm = new Vue();
