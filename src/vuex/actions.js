import axios from 'axios';

const __GHIBLI_URL__ = process.env.VUE_APP_API_URL_GHIBLI;

const axiosInstance = () => {
  return axios.create({
    baseURL: __GHIBLI_URL__,
  })
}

export const fetchGet = async (context, payload) => {
  try {
    context.commit(payload.types.type);
    const response = await axiosInstance(context).get(payload.url);
    context.commit(payload.types.success, response);

    return response;

  } catch (err) {
    context.commit(payload.types.failure, err);
    throw err;
  }
}

export const fetchPost = async (context, payload) => {
  try {
    context.commit(payload.types.type);
    const response = await axiosInstance(context).post(payload.url, payload.data);
    context.commit(payload.types.success, response);

    return response;

  } catch (err) {
    context.commit(payload.types.failure, err);
    throw err;
  }
}

export const fetchPatch = async (context, payload) => {
  try {
    context.commit(payload.types.type);
    const response = await axiosInstance(context).patch(payload.url, payload.data);
    context.commit(payload.types.success, response);

    return response;

  } catch (err) {
    context.commit(payload.types.failure, err);
  }
}

export const fetchDelete = async (context, payload) => {
  try {
    context.commit(payload.types.type);
    const response = await axiosInstance.delete(payload.url, payload.data);
    context.commit(payload.types.success, response);

    return response;

  } catch (err) {
    context.commit(payload.types.failure, err);
  }
}

export const setStoreData = async (context, payload) => {
  context.commit(payload.types.type);
  context.commit(payload.types.success, payload.data);
  return payload.data;
}
