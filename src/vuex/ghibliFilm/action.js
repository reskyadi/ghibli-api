import {  GET_FILM,
          GET_FILM_SUCCESS,
          GET_FILM_FAILURE} from "./constant";

const actions = {
  getGhibliFilms ({ dispatch }) {
    const newPayload = {
      url: 'films',
      types: {
        type: GET_FILM,
        success: GET_FILM_SUCCESS,
        failure: GET_FILM_FAILURE,
      }
    }
    return dispatch('fetchGet', newPayload);
  },
};

export default actions;
