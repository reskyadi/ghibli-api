const getters = {
  getGhibliFilmsData: (state) => state.data,
};

export default getters;
