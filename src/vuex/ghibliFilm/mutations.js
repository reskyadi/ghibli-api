import {  GET_FILM,
          GET_FILM_SUCCESS,
          GET_FILM_FAILURE} from "./constant";

const mutations = {
  [GET_FILM]: () => {
  },
  [GET_FILM_SUCCESS]: (state, payload) => {
    const { data } = payload;
    state.data = data;
  },
  [GET_FILM_FAILURE]: () => {
  },
}

export default mutations;
