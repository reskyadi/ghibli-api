import actions from './action';
import getters from './getters';
import mutations from './mutations';

const state = {
  data: []
};

const module = {
  state,
  mutations,
  actions,
  getters,
};

export default module;
