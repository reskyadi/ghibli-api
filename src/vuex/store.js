import { Store } from 'vuex';

// Import Module
import ghibliFilm from './ghibliFilm';

import { fetchGet, fetchPost, fetchPatch, fetchDelete, setStoreData } from './actions';


const modules = {
  ghibliFilm,
};

// Standar Ajax
const actions = {
  fetchGet,
  fetchPost,
  fetchPatch,
  fetchDelete,
  setStoreData,
}

const createStore = () => {
  return new Store({
    actions,
    modules,
    namespaced: true,
    plugins: []
  });
}

export default createStore;
